package io.cafeliang.kidslanguage;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import io.cafeliang.kidslanguage.nlu.NluApi;
import io.cafeliang.kidslanguage.nlu.NluRequest;
import io.cafeliang.kidslanguage.nlu.NluResponse;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NLUHelper {
    private static final String TAG = NLUHelper.class.getSimpleName();

    private static NLUHelper instance;

    private static final String BASE_URL = "https://dialogflow.googleapis.com/v2/";
    private final NluApi NluApi;
    private String token;


    private NLUHelper() {
        Interceptor headerInterceptor = new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .addHeader("Content-Type", "application/json")
                        .build();
                return chain.proceed(newRequest);
            }
        };

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(headerInterceptor)
                .addInterceptor(loggingInterceptor)
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        NluApi = retrofit.create(NluApi.class);
    }

    public static NLUHelper getInstance() {
        if (instance == null) {
            instance = new NLUHelper();
        }

        return instance;
    }

    public void setToken(String token) {
        this.token = token;
        Log.d(TAG, "setToken: " + token);
    }

    public void detectIntent(NluRequest request, NluListener listener) {
        Log.d(TAG, "detectIntent: ");
        new Thread(() -> {
            String sessionId = UUID.randomUUID().toString();
            try {
                Call<NluResponse> call = NluApi.detectIntent(sessionId, request);
            
                Response<NluResponse> response = call.execute();
                int httpStatusCode = response.code();

                switch (httpStatusCode) {
                    case 200:
                        Log.d(TAG, "detectIntent succeeded: ");
                        NluResponse responseBody = response.body();
                        Log.d(TAG, "response: " + responseBody.getQueryResult().getQueryText());
                        listener.onResponseSucceeded(responseBody);
                        break;
                    default:
                        String errorMessage = "Unexpected HTTP status code: " + httpStatusCode + " " + response.message();
                        listener.onResponseFailed(new IOException(errorMessage));
                        Log.e(TAG, "Fail to detect intent: " + errorMessage);
                        break;
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, "Fail to detect intent: " + e.getLocalizedMessage());
                listener.onResponseFailed(e);
            }

        }).start();
    }

    public interface NluListener {
        void onResponseSucceeded(NluResponse response);
        void onResponseFailed(Exception e);
    }
}
