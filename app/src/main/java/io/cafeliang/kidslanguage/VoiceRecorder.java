/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cafeliang.kidslanguage;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;


/**
 * Continuously records audio and notifies the {@link VoiceRecorder.Callback} when voice (or any
 * sound) is heard.
 *
 * <p>The recorded audio format is always {@link AudioFormat#ENCODING_PCM_16BIT} and
 * {@link AudioFormat#CHANNEL_IN_MONO}. This class will automatically pick the right sample rate
 * for the device. Use {@link #getSampleRate()} to get the selected value.</p>
 */
public class VoiceRecorder {
    private static final String TAG = VoiceRecorder.class.getSimpleName();

    private static final int[] SAMPLE_RATE_CANDIDATES = new int[]{16000, 11025, 22050, 44100};

    private static final int CHANNEL = AudioFormat.CHANNEL_IN_MONO;
    private static final int ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static final int RECORD_PERMISSIONS_REQUEST_CODE = 15623;


//    private static final int AMPLITUDE_THRESHOLD = 1500;
    private static final int AMPLITUDE_THRESHOLD = 4000;

    private static final int SPEECH_TIMEOUT_MILLIS = 2000;
    private static final int MAX_SPEECH_LENGTH_MILLIS = 30 * 1000;

    public static abstract class Callback {

        /**
         * Called when the recorder starts hearing voice.
         */
        public void onVoiceStart() {
        }

        /**
         * Called when the recorder is hearing voice.
         *
         * @param data The audio data in {@link AudioFormat#ENCODING_PCM_16BIT}.
         * @param size The size of the actual data in {@code data}.
         */
        public void onVoice(byte[] data, int size) {
        }

        /**
         * Called when the recorder stops hearing voice.
         */
        public void onVoiceEnd() {
        }
    }

    private final Callback mCallback;

    private AudioRecord mAudioRecord;

    private Thread mThread;

    private byte[] mBuffer;

    private final Object mLock = new Object();

    /** The timestamp of the last time that voice is heard. */
    private long mLastVoiceHeardMillis = Long.MAX_VALUE;

    /** The timestamp when the current voice is started. */
    private long mVoiceStartedMillis;

    public VoiceRecorder(@NonNull Callback callback) {
        mCallback = callback;
    }


    public boolean hasRequiredPermissions(Context context) {
        int recordAudioPermissionCheck = ContextCompat.checkSelfPermission(
                context, Manifest.permission.RECORD_AUDIO);
        int writeExternalStoragePermissionCheck = ContextCompat.checkSelfPermission(
                context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return recordAudioPermissionCheck == PackageManager.PERMISSION_GRANTED &&
                writeExternalStoragePermissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public void requestRequiredPermissions(Activity activity) {
        activity.requestPermissions(
                new String[]{
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                RECORD_PERMISSIONS_REQUEST_CODE
        );
    }


    /**
     * Starts recording audio.
     *
     * <p>The caller is responsible for calling {@link #stop()} later.</p>
     */
    public void start() {
        // Stop recording if it is currently ongoing.
        stop();
        // Try to create a new recording session.
        mAudioRecord = createAudioRecord();
        if (mAudioRecord == null) {
            throw new RuntimeException("Cannot instantiate VoiceRecorder");
        }
        // Start recording.
        mAudioRecord.startRecording();
        // Start processing the captured audio.
        mThread = new Thread(new ProcessVoice());
        mThread.start();
    }

    /**
     * Stops recording audio.
     */
    public void stop() {
        synchronized (mLock) {
            dismiss();
            if (mThread != null) {
                mThread.interrupt();
                mThread = null;
            }
            if (mAudioRecord != null) {
                mAudioRecord.stop();
                mAudioRecord.release();
                mAudioRecord = null;
            }
            mBuffer = null;
        }
    }

    /**
     * Dismisses the currently ongoing utterance.
     */
    public void dismiss() {
        if (mLastVoiceHeardMillis != Long.MAX_VALUE) {
            mLastVoiceHeardMillis = Long.MAX_VALUE;
            mCallback.onVoiceEnd();
        }
    }

    /**
     * Retrieves the sample rate currently used to record audio.
     *
     * @return The sample rate of recorded audio.
     */
    public int getSampleRate() {
        if (mAudioRecord != null) {
            return mAudioRecord.getSampleRate();
        }
        return 0;
    }

    /**
     * Creates a new {@link AudioRecord}.
     *
     * @return A newly created {@link AudioRecord}, or null if it cannot be created (missing
     * permissions?).
     */
    private AudioRecord createAudioRecord() {
        for (int sampleRate : SAMPLE_RATE_CANDIDATES) {
            final int sizeInBytes = AudioRecord.getMinBufferSize(sampleRate, CHANNEL, ENCODING);
            if (sizeInBytes == AudioRecord.ERROR_BAD_VALUE) {
                continue;
            }
            final AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                                                            sampleRate, CHANNEL, ENCODING, sizeInBytes);
            if (audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {
                mBuffer = new byte[sizeInBytes];
                return audioRecord;
            } else {
                audioRecord.release();
            }
        }
        return null;
    }


    private String raw_file_path;
    private String wav_file_path;
    private BufferedOutputStream outputStream;

    File wavFile;


    public void setFilePath(String filePath){
        Log.d(TAG, "setFilePath: " + filePath);
        raw_file_path = Environment.getExternalStorageDirectory().getPath() + "/kidslanguage/" + filePath + ".raw";
        wav_file_path = Environment.getExternalStorageDirectory().getPath() + "/kidslanguage/" +  filePath + ".wav";

        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(raw_file_path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Couldn't find file: " + raw_file_path, e);
        }
    }

    public File getRecordingFile() {
        return wavFile;
    }

    private boolean record = true;

    private boolean stopListeningAfterNoHearing = false;
    public void setStopListeningAfterNoHearing(boolean stopListening) {
        this.stopListeningAfterNoHearing = stopListening;
    }

    public void setShouldSaveRecording(boolean record) {
        this.record = record;
    }

    public boolean getShouldSaveRecording() {
        return this.record;
    }

    /**
     * Continuously processes the captured audio and notifies {@link #mCallback} of corresponding
     * events.
     */
    private class ProcessVoice implements Runnable {
        private static final int SAMPLE_RATE_IN_HZ = 16000;

        @Override
        public void run() {
            while (true) {
                synchronized (mLock) {
                    if (Thread.currentThread().isInterrupted()) {
                        break;
                    }
                    final int size = mAudioRecord.read(mBuffer, 0, mBuffer.length);
                    final long now = System.currentTimeMillis();
                    if (isHearingVoice(mBuffer, size)) {
                        Log.d(TAG, "isHearingVoice:");
                        if (mLastVoiceHeardMillis == Long.MAX_VALUE) {
                            mVoiceStartedMillis = now;
                            mCallback.onVoiceStart();

                        }
                        mCallback.onVoice(mBuffer, size);

                        if (record) {
                            try {
                                outputStream.write(mBuffer, 0, mBuffer.length);
                            } catch (IOException e) {
                                Log.e(TAG, "Couldn't save data", e);
                            }
                        }

                        mLastVoiceHeardMillis = now;
                        if (now - mVoiceStartedMillis > MAX_SPEECH_LENGTH_MILLIS) {
                            end();
                        }
                    } else if (mLastVoiceHeardMillis != Long.MAX_VALUE) {
                        if (stopListeningAfterNoHearing) {
                            Log.d(TAG, "stopListeningAfterNoHearing: ");
                            end();
                        } else {
                            mCallback.onVoice(mBuffer, size);

                            try {
                                outputStream.write(mBuffer, 0, mBuffer.length);
                            } catch (IOException e) {
                                Log.w(TAG, "Couldn't save data", e);
                            }

                            if (now - mLastVoiceHeardMillis > SPEECH_TIMEOUT_MILLIS) {
                                end();
                            }
                        }
                    }
                }
            }
        }

        private void end() {
            // After the client calls stopRecording(), this method processes the recorded audio.

            if (record) {
                try {
                    if (outputStream != null) {
                        outputStream.close();

                        Log.v(TAG, "Recording stopped");

                        File rawFile = new File(raw_file_path);
                        wavFile = new File(wav_file_path);
                        saveAsWave(rawFile, wavFile);
                    }
                } catch (IOException e) {
                    Log.e(TAG, "File error", e);
                }
            }

            mLastVoiceHeardMillis = Long.MAX_VALUE;
            mCallback.onVoiceEnd();
        }

        private boolean isHearingVoice(byte[] buffer, int size) {
            for (int i = 0; i < size - 1; i += 2) {
                // The buffer has LINEAR16 in little endian.
                int s = buffer[i + 1];
                if (s < 0) s *= -1;
                s <<= 8;
                s += Math.abs(buffer[i]);
                if (s > AMPLITUDE_THRESHOLD) {
                    return true;
                }
            }
            return false;
        }

        private void saveAsWave(final File rawFile, final File waveFile) throws IOException {
            byte[] rawData = new byte[(int) rawFile.length()];
            try (DataInputStream input = new DataInputStream(new FileInputStream(rawFile))) {
                int readBytes;
                do {
                    readBytes = input.read(rawData);
                }
                while(readBytes != -1);
            }

            try (DataOutputStream output = new DataOutputStream(new FileOutputStream(waveFile))) {
                // WAVE specification
                Charset asciiCharset = Charset.forName("US-ASCII");
                // Chunk ID: "RIFF" string in US-ASCII charset—4 bytes Big Endian
                output.write("RIFF".getBytes(asciiCharset));
                // Chunk size: The size of the actual sound data plus the rest
                //             of this header (36 bytes)—4 bytes Little Endian
                output.write(convertToLittleEndian(36 + rawData.length));
                // Format: "WAVE" string in US-ASCII charset—4 bytes Big Endian
                output.write("WAVE".getBytes(asciiCharset));
                // Subchunk 1 ID: "fmt " string in US-ASCII charset—4 bytes Big Endian
                output.write("fmt ".getBytes(asciiCharset));
                // Subchunk 1 size: The size of the subchunk.
                //                  It must be 16 for PCM—4 bytes Little Endian
                output.write(convertToLittleEndian(16));
                // Audio format: Use 1 for PCM—2 bytes Little Endian
                output.write(convertToLittleEndian((short)1));
                // Number of channels: This sample only supports one channel—2 bytes Little Endian
                output.write(convertToLittleEndian((short)1));
                // Sample rate: The sample rate in hertz—4 bytes Little Endian
                output.write(convertToLittleEndian(SAMPLE_RATE_IN_HZ));
                // Bit rate: SampleRate * NumChannels * BitsPerSample/8—4 bytes Little Endian
                output.write(convertToLittleEndian(SAMPLE_RATE_IN_HZ * 2));
                // Block align: NumChannels * BitsPerSample/8—2 bytes Little Endian
                output.write(convertToLittleEndian((short)2));
                // Bits per sample: 16 bits—2 bytes Little Endian
                output.write(convertToLittleEndian((short)16));
                // Subchunk 2 ID: "fmt " string in US-ASCII charset—4 bytes Big Endian
                output.write("data".getBytes(asciiCharset));
                // Subchunk 2 size: The size of the actual audio data—4 bytes Little Endian
                output.write(convertToLittleEndian(rawData.length));

                // Audio data:  Sound data bytes—Little Endian
                short[] rawShorts = new short[rawData.length / 2];
                ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(rawShorts);
                ByteBuffer bytes = ByteBuffer.allocate(rawData.length);
                for (short s : rawShorts) {
                    bytes.putShort(s);
                }

                output.write(readFile(rawFile));
            }
        }

        private byte[] convertToLittleEndian(Object value) {
            int size;
            if(value.getClass().equals(Integer.class)) {
                size = 4;
            } else if (value.getClass().equals(Short.class)) {
                size = 2;
            } else {
                throw new IllegalArgumentException("Only int and short types are supported");
            }

            byte[] littleEndianBytes = new byte[size];
            ByteBuffer byteBuffer = ByteBuffer.allocate(size);
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

            if(value.getClass().equals(Integer.class)) {
                byteBuffer.putInt((int)value);
            } else if (value.getClass().equals(Short.class)) {
                byteBuffer.putShort((short)value);
            }

            byteBuffer.flip();
            byteBuffer.get(littleEndianBytes);

            return littleEndianBytes;
        }

        private byte[] readFile(File f) throws IOException {
            int size = (int) f.length();
            byte bytes[] = new byte[size];
            byte tmpBuff[] = new byte[size];
            try (FileInputStream fis = new FileInputStream(f)) {
                int read = fis.read(bytes, 0, size);
                if (read < size) {
                    int remain = size - read;
                    while (remain > 0) {
                        read = fis.read(tmpBuff, 0, remain);
                        System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
                        remain -= read;
                    }
                }
            }
            return bytes;
        }
    }
}
