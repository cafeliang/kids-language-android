package io.cafeliang.kidslanguage.dialogmanager;

public class ImageResponse {
    private String key;
    private String uri;
    private int delay_time;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getDelayTime() {
        return delay_time;
    }
}
