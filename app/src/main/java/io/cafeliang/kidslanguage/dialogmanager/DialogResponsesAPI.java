package io.cafeliang.kidslanguage.dialogmanager;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface DialogResponsesAPI {

    @POST("./")
    Call<DialogResponse> getDialogResponse(@Body DialogRequest dialogResponseRequest);
}
