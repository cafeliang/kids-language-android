package io.cafeliang.kidslanguage.dialogmanager;

public class Word {
    String en;
    String es;

    public String getEN() {
        return en;
    }

    public String getES() {
        return es;
    }
}
