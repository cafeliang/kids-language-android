package io.cafeliang.kidslanguage.dialogmanager;

import java.util.List;

public class DisplayResponse {
    private String text;
    private List<ImageResponse> images;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ImageResponse> getImages() {
        return images;
    }

    public void setImages(List<ImageResponse> images) {
        this.images = images;
    }
}
