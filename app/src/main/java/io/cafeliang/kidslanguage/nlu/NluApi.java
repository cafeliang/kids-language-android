package io.cafeliang.kidslanguage.nlu;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface NluApi {

//    @POST("{sessionPrefix}/{sessionId}:detectIntent")
//    Call<NluResponse> detectIntent(@Path("sessionPrefix") String prefix, @Path("sessionId") String sessionId, @Body NluRequest nluRequest);

    @POST("projects/machine-learning-238116/agent/sessions/{sessionId}:detectIntent")
    Call<NluResponse> detectIntent(@Path("sessionId") String sessionId, @Body NluRequest nluRequest);
}
