package io.cafeliang.kidslanguage.nlu;

public class NluResponse {
    QueryResult queryResult;

    public QueryResult getQueryResult() {
        return queryResult;
    }

    public void setQueryResult(QueryResult result) {
        this.queryResult = result;
    }
}
