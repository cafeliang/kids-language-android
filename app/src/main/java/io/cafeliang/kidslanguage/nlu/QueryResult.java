package io.cafeliang.kidslanguage.nlu;


import java.util.Map;

public class QueryResult {
    String queryText;
    Map<String, String> parameters;
    Intent intent;

    public String getQueryText() {
        return queryText;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getIntent() {
        return intent.getDisplayName();
    }
}
